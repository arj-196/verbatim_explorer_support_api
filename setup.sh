#!/usr/bin/env bash

# dependency repositories
REP_INSIGHTS="https://arj-196@bitbucket.org/arj-196/insight_extractor.git"
ARR=(1,2)

function setupDependencies {
    echo "delete directory bin"
    rm -rf bin
    echo "setting up dependencies"
    mkdir bin
    echo "" > bin/__init__.py

    # setup
    # insight_extractor
    cd bin
    git clone ${REP_INSIGHTS}
    cp -rf insight_extractor/insight .
    cp -rf insight_extractor/debugger .
    cp -rf insight_extractor/vanalyze .
    rm -rf insight_extractor

    # finally
    cd ..
}

function fntest {
    echo "tests"
}

echo "explorer support setup tools"
if [ $1 == "setup" ]; then
    setupDependencies
elif [ $1 == "test" ]; then
    fntest
else
    echo "ERR: option not recognized"
fi


# usage
# sh setup.sh setup

