from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import sys, traceback
import explorer_support.resources as rs
import json
from unitext.insight import export
import os

# init models
exporter_tagged = export.ExportTagged()
exporter_insight = export.ExportInsight()


@api_view(['POST'])
def export_tags(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'tags' in params and 'topics' in params:
            topics = json.loads(params['topics'])
            tags = json.loads(params['tags'])

            try:
                filename = exporter_tagged.export_tags(topics, tags, 'exports/')
                return Response({"s": True, "f": os.path.abspath(__file__ + "/../../../") + "/exports/" + filename},
                                status=status.HTTP_200_OK)

            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response({'s': False}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def export_insights(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'insightids' in params and 'topics' in params and 'nbPredicted' in params:
            topics = json.loads(params['topics'])
            insightids = json.loads(params['insightids'])
            nbpredicted = int(params['nbPredicted'])

            try:
                filename = exporter_insight.export_insights(topics, insightids, 'exports/', nbverbatims=nbpredicted)
                return Response({"s": True, "f": os.path.abspath(__file__ + "/../../../") + "/exports/" + filename},
                                status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response({'s': False}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)
