import json
import sys, traceback
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

import error
from unitext.vanalyze.VAnalyzer import BaseAnalyzer
from explorer_support import resources as rs


@api_view(['POST'])
def get_similarities(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'v' in params:
            try:
                verbatimIds = json.loads(params['v'])

                # declare verbatim analyzer
                verbatim_analyzer = BaseAnalyzer(rs.HOST, params['lang'], 10)

                # get similiarities between verbatims
                similarities = verbatim_analyzer.get_similarities(verbatimIds)

                r = {
                    "similarities": similarities
                }
                return Response(r, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": error.NOVERBATIMLSIT}, status=status.HTTP_204_NO_CONTENT)
