from django.conf.urls import patterns, include, url
import conceptnet.views as conceptnetviews
import mverbatims.views as mverbatimsviews
import dataset.views as datasetviews
import statistics.views as statsview
import wordcloud.views as wordcloudview
import modal.views as modalview
import export.views as exportview
import statistics_img.views as imgstatsviews
import tmodel.views as tmodelviews

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^conceptnet/$', conceptnetviews.get_concept),

    url(r'^mv/similarities/$', mverbatimsviews.get_similarities),

    url(r'^dataset/new/$', datasetviews.process_dataset),

    # sample dataset
    url(r'^dataset/sample/$', datasetviews.sample_dataset),

    # statistics
    url(r'^stats/emotion/$', statsview.get_emotion_stats),
    url(r'^stats/ner/$', statsview.get_ner_stats),

    # wordclouds
    url(r'^stats/wordcloud/$', wordcloudview.get_words),

    # export dataset
    url(r'^stats/exportdataset/$', statsview.export_dataset),

    # aggregate search
    url(r'^stats/searchaggregate/$', statsview.aggregate_search),

    # modal
    url(r'^modals/compile/insight/$', modalview.compile_insight_modal),
    url(r'^modals/compile/insight/sindex/$', modalview.compile_insight_sindex),

    # export
    url(r'^export/tags/$', exportview.export_tags),
    url(r'^export/insights/$', exportview.export_insights),

    # images wordlcoud
    url(r'^images/stats/wordcloud/$', imgstatsviews.get_wordcloud),

    # tmodel
    url(r'^tmodel/createdataset/$', tmodelviews.create_dataset),


    # test ping
    url(r'^test1/$', wordcloudview.test_ping),

)
