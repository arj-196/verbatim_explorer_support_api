import json
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import error
import os
from unitext.insight import statistics
import sys, traceback
import explorer_support.resources as rs

# init models
generalstats = statistics.GeneralStatistics()
nerStats = statistics.NERStatistics()
exporter = statistics.ExportStatistics()

@api_view(['POST'])
def get_emotion_stats(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'lang' in params and 'cl' in params and \
                        'lm' in params and 'os' in params and 'sm' in params and 't' in params:
            # variables
            commandList = json.loads(params['cl'])
            languageMap = json.loads(params['lm'])
            orStatement = json.loads(params['os'])
            searchmode = params['sm']
            lang = params['lang']
            topic = params['t']
            try:
                # get statistics
                stats = generalstats.get_stats(searchmode, topic, orStatement, languageMap)
                return Response(stats, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_ner_stats(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'lang' in params and 'cl' in params and \
                        'lm' in params and 'os' in params and 'sm' in params and 't' in params:
            # variables
            commandList = json.loads(params['cl'])
            languageMap = json.loads(params['lm'])
            orStatement = json.loads(params['os'])
            searchmode = params['sm']
            lang = params['lang']
            topic = params['t']
            try:
                # get statistics
                stats = nerStats.get_stats(searchmode, topic, orStatement, languageMap)
                return Response(stats, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def export_dataset(request):
    if request.method == 'POST':

        params = rs.get_params(request)
        if 'lang' in params and 'cl' in params and \
                        'lm' in params and 'os' in params and 'sm' in params and 't' in params:
            # variables
            commandList = json.loads(params['cl'])
            languageMap = json.loads(params['lm'])
            orStatement = json.loads(params['os'])
            searchmode = params['sm']
            lang = params['lang']
            topic = params['t']
            try:
                # get statistics
                filename = exporter.export('exports/', searchmode, topic, orStatement, languageMap)
                return Response({"f": os.path.abspath(__file__ + "/../../../") + "/exports/" + filename},
                                status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def aggregate_search(request):
    if request.method == 'POST':

        params = rs.get_params(request)
        if 't' in params and 'sid' in params:
            # variables
            sessionid = params['sid']
            topic = params['t']
            searchengine = statistics.SearchEngineStatistics()
            ifloadmore = params['ifloadmore']

            try:
                # get statistics
                responseid = searchengine.doSearch(sessionid, topic, ifloadmore)
                return Response({"r": responseid}, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)
