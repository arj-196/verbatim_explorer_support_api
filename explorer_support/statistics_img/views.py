import json
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import error
from uniimage.insight import statistics
import sys, traceback, os
import explorer_support.resources as rs

# init models
wordcloudStats = statistics.WordCloudStatistics()


@api_view(['POST'])
def get_wordcloud(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'sm' in params and 't' in params and 'os' in params:
            try:
                searchmode = params['sm']
                targetmode = params['tm']
                topic = params['t']
                orStatement = json.loads(params['os'])
                # get statistics

                stats = wordcloudStats.get_stats(searchmode, targetmode, topic, orStatement)
                return Response(stats, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def test_ping(request):
    if request.method == 'GET':
        return Response({'r': 'WORKS : TEST 1'}, status=status.HTTP_200_OK)
