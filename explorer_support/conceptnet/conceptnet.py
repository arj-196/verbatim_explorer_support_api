from unitext.insight.conceptapi import ConceptNetAPI
conceptnetapi = ConceptNetAPI()


class ConceptNetWrapper(object):
    def get(self, c, lang):
        return conceptnetapi.get_concepts(c, lang)
