from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import error
import sys, traceback
from conceptnet import ConceptNetWrapper
import explorer_support.resources as rs
conceptnetapi = ConceptNetWrapper()


@api_view(['POST'])
def get_concept(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'c' in params and 'l' in params:
            try:
                r = conceptnetapi.get(params['c'], params['l'])
                # r = {'a': params['c']}
                return Response({"r": r}, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": error.NOCONCEPT}, status=status.HTTP_204_NO_CONTENT)
