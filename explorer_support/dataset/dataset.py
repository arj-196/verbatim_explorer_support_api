# -*- coding: utf-8 -*-
import sys, traceback
from pymongo import MongoClient
import explorer_support.resources as rs
import os
import unitext.insight.insightextractor as extractor
from unitext.insight.indexer import WordCloudIndexer
_client = MongoClient(rs.HOST)


def is_valid(title, filepath):
    s = True
    m = ""
    # title
    titles = _client.indexes.verbatims.distinct("title")
    if title.strip() not in titles:
        if os.path.exists(filepath):
            a = filepath.split(".")
            if a[len(a) - 1] == "verbatim":
                s = True
            else:
                s = False
                m = "File is not .verbatim FORMAT"
        else:
            s = False
            m = "File does not exist"
    else:
        s = False
        m = "Title Not Distinct"

    return {'s': s, 'm': m}


def delete_verbatim_file(filepath):
    if os.path.exists(filepath):
        os.remove(filepath)


def process_dataset(title, filepath):
    isvalid = is_valid(title, filepath)
    if isvalid['s']:
        # TODO SERIOUS ENCODING ISSUES!
        print "starting indexing on", title
        simpleexactor = extractor.BaseInsightExtactor(lang='fr')
        wordcloudindexer = WordCloudIndexer()
        content = open(filepath, 'r').readlines()
        for line in content:
            try:
                string = line.split('\t')[0].decode('utf8')
                # string = string.encode('utf8')
                # extract insights
                simpleexactor.extract(string, title,
                                      ifsave=True,
                                      ifconcept=True, ifkeyword=True,
                                      ifpos=True, ifsentiment=True, ifterm=True,
                                      ifemotion=True
                                      )
                # index wordcloud data
                wordcloudindexer.index(title)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)
    else:
        delete_verbatim_file(filepath)
    # finally
    return isvalid


# f = '/Users/arj/www/verbatim_explorer/public/tmp/uploads/1452284587992.verbatim'
# t = 'bp'
# print process_dataset(t, f)
