from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
import sys, traceback
import dataset
import explorer_support.resources as rs
from unitext.insight import insightextractor as extractor


@api_view(['POST'])
def process_dataset(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'f' in params and 't' in params:
            try:
                r = dataset.process_dataset(params['t'], params['f'])
                return Response(r, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def sample_dataset(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'p' in params and 't' in params and 'l' in params and 'f' in params and 'c' in params and 'u' in params:
            try:
                project = params['p']
                topic = params['t']
                language = params['l']
                filename = params['f']
                company = params['c']
                user = params['u']

                manager = extractor.InsightExtractorManager(
                    inputfile=filename, topic=topic, project=project, company=company, user=user, lang=language,
                    extractor=extractor.BaseInsightExtactor
                )
                res = manager.preview('./tmp/')
                if res['s']:
                    dumpfile = res['r']
                    return Response({'s': True, 'r': dumpfile}, status=status.HTTP_200_OK)
                else:
                    return Response({'s': False, 'm': 'BADFILE?m=NOTUTF8'}, status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                traceback.print_exc(file=sys.stdout)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)
