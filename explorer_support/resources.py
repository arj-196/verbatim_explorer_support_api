HOST = "151.80.41.13:27018"
# HOST = "localhost"
# STRATEGY = 1
STRATEGY = 2

ERRORLOGGERMAILADDRESS = "insightE@thedatastrategy.com"
from unitools.mailer import mailer


def get_params(request):
    if STRATEGY == 1:
        return request.QUERY_PARAMS
    elif STRATEGY == 2:
        return request.query_params


def log_error(e):
    Mailer = mailer.Mailer()
    Mailer.send(
        ERRORLOGGERMAILADDRESS,
        ERRORLOGGERMAILADDRESS,
        "ERROR LOG : SUPPORT API",
        str(e),
        ifBodyDecorate=False
    )

