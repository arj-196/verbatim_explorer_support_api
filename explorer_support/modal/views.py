import json
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import error
import sys, traceback
import explorer_support.resources as rs
from unitext.insight import modals
from unitext.insight import resources as rsi
from pymongo import MongoClient


@api_view(['POST'])
def compile_insight_modal(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'id' in params and 't' in params:
            insightid = params['id']
            topic = params['t']
            # fetch insight
            client = MongoClient(rsi.HOST)
            insight = client.indexes.insights.find_one({'_id': insightid})
            del client
            if not insight:
                return Response({"message": "Insight not found"}, status=status.HTTP_204_NO_CONTENT)
            else:
                try:
                    # train and predict
                    insightmodal = modals.AutomatedInsightModal()
                    insightmodal.train_on(insight)
                    insightmodal.predict(insight, topic)
                    return Response({'s': True}, status=status.HTTP_200_OK)
                except:
                    e = sys.exc_info()[0]
                    print ("ERROR %s", e)
                    rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                    traceback.print_exc(file=sys.stdout)
                    return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def compile_insight_sindex(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'id' in params and 't' in params:
            insightid = params['id']
            topic = params['t']
            # fetch insight
            client = MongoClient(rsi.HOST)
            insight = client.indexes.insights.find_one({'_id': insightid})
            del client

            if not insight:
                return Response({"message": "Insight not found"}, status=status.HTTP_204_NO_CONTENT)
            else:
                try:
                    # train and predict
                    insightmodal = modals.AutomatedInsightModal()
                    insightmodal.train_on(insight)
                    s_index = insightmodal.get_s_index(insight, topic)
                    return Response({'s': True, 'r': s_index}, status=status.HTTP_200_OK)
                except:
                    e = sys.exc_info()[0]
                    print ("ERROR %s", e)
                    rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                    traceback.print_exc(file=sys.stdout)
                    return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)
