import uuid
from pymongo import MongoClient
from pymongo import errors as pyerrors
import unitext.insight.resources as rs
from unitext.insight.indexer import WordCloudIndexer


class TModelManager(object):
    def __init__(self):
        self.client = MongoClient(rs.HOST)

    def create_dataset(self, topic_id, topic_name, project_id, user_id):
        # get verbatims from generated topic
        topic_verbatims = [d for d in self.client.topics.verbatimtopics.find(
            {'topic_id': topic_id}
        )]
        verbatim_ids = [d['verbatim_id'] for d in topic_verbatims]
        if verbatim_ids:
            gen_dataset_name = 'TopSeg_' + topic_name

            # get meta data
            project = self.client.client.projects.find_one({'_id': project_id})
            topic = self.client.indexes.settings.find_one({'project': project_id})
            # update dataset settings
            try:
                self.client.indexes.settings.insert(dict(
                    _id=topic_id,
                    company=project['company'],
                    dir='unnamed',
                    lang=topic['lang'],  # taking first found language
                    project=project_id,
                    topic=gen_dataset_name,
                    users=[user_id]
                ))
            except pyerrors.DuplicateKeyError:
                pass

            verbatims = [d for d in self.client.indexes.verbatims.find({'_id': {'$in': verbatim_ids}})]
            topic_meta = self.get_topic_meta_data(verbatim_ids)

            self._insert_to_db(topic_id, gen_dataset_name, topic_verbatims, verbatims, topic_meta)
            self._index_new_collection(gen_dataset_name)
        else:
            return True, "ERR: no verbatims found"
        return False, None

    def _insert_to_db(self, topic_id, gen_dataset_name, topic_verbatims, verbatims, meta):

        # getting verbatim weights
        verbatim_ids = []
        verbatim_weights = {}
        for d in topic_verbatims:
            _id = d['verbatim_id']
            verbatim_ids.append(_id)
            verbatim_weights[_id] = d['weight']

        # inserting verbatims
        id_mapping = {}
        for d in verbatims:
            _id = d['__id'] = d['_id']
            d['weight'] = verbatim_weights[_id]
            d['topic'] = gen_dataset_name

            # generate new id
            d['_id'] = str(uuid.uuid4()) + '_' + topic_id
            # mapping old id to new id
            id_mapping[_id] = d['_id']

            try:
                self.client.indexes.verbatims.insert(d)
            except pyerrors.DuplicateKeyError:
                pass

        # inserting meta data
        for col_type in meta.keys():
            docs_to_insert = []
            for d in meta[col_type]:
                _id = d['__id'] = d['_id']
                if _id in id_mapping:
                    d['_id'] = id_mapping[_id]
                    d['enabled'] = True  # by default turn everything on again
                    d['topic'] = gen_dataset_name

                    docs_to_insert.append(d)

                    # adding topic name to meta collections
                    if col_type in d:
                        search_terms = d[col_type]
                    elif rs.COLLECTION_FIELD_MAP[col_type]['word'] in d:
                        search_terms = d[rs.COLLECTION_FIELD_MAP[col_type]['word']]
                    else:
                        search_terms = None

                    if search_terms:
                        meta_docs = [d for d in self.client.indexes[col_type].find(
                            {rs.COLLECTION_FIELD_MAP[col_type]['word']: {'$in': search_terms}}
                        )]
                        items_to_update = []
                        for d in meta_docs:
                            if 'topics' in d:
                                if gen_dataset_name not in d['topics']:
                                    items_to_update.append(d['_id'])

                        if items_to_update:
                            self.client.indexes[col_type].update(
                                {'_id': {'$in': items_to_update}},
                                {'$push': {'topics': gen_dataset_name}}
                            )

            # insert in batch
            col = self.client.indexes['verbatim' + col_type]
            for chunk in self.chunks(docs_to_insert, 50):
                try:
                    col.insert(chunk)
                except pyerrors.DuplicateKeyError:
                    pass

        # insert ners
        verbatim_ners = [d for d in self.client.indexes.verbatimners.find({'_id': {'$in': verbatim_ids}})]
        items_to_insert = []
        for d in verbatim_ners:
            _id = d['__id'] = d['_id']
            if _id in id_mapping:
                d['_id'] = id_mapping[_id]
                d['enabled'] = True  # by default turn everything on again
                d['topic'] = gen_dataset_name

                items_to_insert.append(d)

        for chunk in self.chunks(items_to_insert, 50):
            try:
                self.client.indexes.verbatimners.insert(chunk)
            except pyerrors.DuplicateKeyError:
                pass

    @staticmethod
    def _index_new_collection(gen_dataset_name):
        wordcloudIndexer = WordCloudIndexer()
        wordcloudIndexer.index(gen_dataset_name)

    def get_topic_meta_data(self, verbatim_ids):
        context = {}
        for col_type in rs.COLLECTION_NAME_LIST:
            col = self.client.indexes['verbatim' + col_type]
            context[col_type] = [d for d in col.find({'_id': {'$in': verbatim_ids}})]
        return context

    @staticmethod
    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]
