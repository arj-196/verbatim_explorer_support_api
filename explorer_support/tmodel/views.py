from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import error
import sys, traceback, os
import explorer_support.resources as rs
import tmodel

manager = tmodel.TModelManager()

@api_view(['POST'])
def create_dataset(request):
    if request.method == 'POST':
        params = rs.get_params(request)
        if 'tid' in params and 'tname' in params and 'pid' in params and 'uid' in params:
            try:
                s, m = manager.create_dataset(params['tid'], params['tname'], params['pid'], params['uid'])
                if s:
                    return Response(dict(s=True), status=status.HTTP_200_OK)
                else:
                    return Response(dict(s=False, m=m), status=status.HTTP_200_OK)
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)
                rs.log_error(__file__ + "<br><br>" + "<br>".join(traceback.format_exc().splitlines()))
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"message": "ALL PARAMETERS NOT DEFINED"}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def test_ping(request):
    if request.method == 'GET':
        return Response({'r': 'WORKS : TEST 1'}, status=status.HTTP_200_OK)
