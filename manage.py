#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # Add Local Resource file Location
    PATH_LR = os.getcwd() + '/local_resources.py'
    if not os.path.exists(PATH_LR):
        raise Exception("File local_resources.py is not defined")
    os.environ['PATH_RESOURCES'] = PATH_LR

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "explorer_support.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
